class Post < ActiveRecord::Base
  default_scope order('created_at DESC')
  validates :title, :tags, :author, :body, presence: true
  
  def body_html
    self.body
  end
  
  def url
    d = created_at
    "/#{d.year}/#{d.month}/#{d.day}/#{slug}/"
  end
  
  def full_url
    MyBlog.url_base.gsub(/\/$/, '') + url
  end
  
  def linked_tags
    self.tags.split.inject([]) do |accum, tag|
      accum << "<a href=\"/tags/#{tag}\">#{tag}</a>"
    end.join(" ")
  end
    
  def self.make_slug(title)
    title.to_ascii.parameterize
  end

  def summary
    #@summary = self.body[/(\s*\S+){#{200}}/]
    @summary ||= self.body.match(/(.{200}.*?\n)/m)
    @summary || self.body
  end
  
  def summary_html
    summary.to_s
  end
  
  def more?
    @more ||= self.body.match(/.{200}.*?\n(.*)/m)
    @more
  end
  
  
end
