class CreatePosts < ActiveRecord::Migration
  def up
    create_table :posts do |t|
      t.text :title
      t.text :body
      t.text :slug
      t.text :tags
      t.string :author
      t.timestamps
    end
  end

  def down
    drop_table :posts
  end
end
