# encoding: UTF-8
require 'rubygems'
require 'sinatra'
require "sinatra/activerecord"
require 'unidecoder'
require 'rdiscount'
require 'sinatra/assetpack'
require 'sass'

set :root, File.dirname(__FILE__) # You must set app root
set :environment, ENV["RACK_ENV"] || "development"

configure :development do
  set :database, "sqlite3:///blog.db"
end


configure :production do
  db = URI.parse('postgres://')

  ActiveRecord::Base.establish_connection(
    :adapter  => db.scheme == 'postgres' ? 'postgresql' : db.scheme,
    :host     => db.host,
    :username => db.user,
    :password => db.password,
    :database => db.path[1..-1],
    :encoding => 'utf8'
  )
end



configure do
  require 'ostruct'
  MyBlog = OpenStruct.new(
    title: 'Προγραμμάτισε και εσύ',
    author: 'Ioannis - Andrea Philippas',
    url_base: 'http://www.programmatise.gr/',
    admin_password: '',
    admin_cookie_key: '',
    admin_cookie_value: '',
    disqus_shortname: nil
  )

  assets do
    serve '/images', from: 'assets/images'
    serve '/css', :from => 'assets/stylesheets'
    serve '/js', :from => 'assets/javascripts'

    js :app, '/js/app.js', [
      '/js/Markdown.Converter.js',
      '/js/Markdown.Editor.js',
      '/js/Markdown.Sanitizer.js',
    ]

    css :application, '/css/application.css', [
    '/css/wmd.css',
    '/css/main.css',
    ]

  js_compression  :uglify
  css_compression :sass

  end
end



#helpers
helpers do
  
  def title
    if @title
      @title + " | " + MyBlog.title
    else
      MyBlog.title
    end
  end
  
  def link_to(url,text=url,opts={})
    attributes = ""
    opts.each { |key,value| attributes << key.to_s << "=\"" << value << "\" "}
    "<a href=\"#{url}\" #{attributes}>#{text}</a>"
  end
  
  def admin?
    	request.cookies[MyBlog.admin_cookie_key] == MyBlog.admin_cookie_value
  end
  
  def auth
    halt [ 401, 'Not authorized' ] unless admin?
  end
  
  def pretty_date(time)
    time.strftime("%d %b %Y")
  end

  def m(string)
    RDiscount.new(string).to_html
  end
end

$LOAD_PATH.unshift(File.dirname(__FILE__) + '/lib')
require 'post'

#Routes
not_found do
  erb :'404'
end

get '/admin' do
  erb :admin
end

post '/admin' do
  response.set_cookie(MyBlog.admin_cookie_key, MyBlog.admin_cookie_value) if params[:password] == MyBlog.admin_password
  redirect '/'
end

get '/contact' do
  erb :contact
end

post '/contact' do 
  require 'pony'
  Pony.mail(
    :from => params[:name] + "<" + params[:email] + ">",
    :to => 'admin@programmatise.gr',
    :subject => params[:name] + " has contacted you",
    :body => params[:message],
    :via => :smtp,
    :via_options => { 
      :address              => 'smtp.gmail.com',
      :port                 => '587',
      :enable_starttls_auto => true,
      :user_name            => 'programmatise.gr@gmail.com',
      :password             => '',
      :authentication       => :plain, # :plain, :login, :cram_md5, no auth by default
      :domain               => "localhost.localdomain" # the HELO domain provided by the client to the server
    })
    redirect '/success' 
end

get '/success' do
  erb :success
end

get '/:year/:month/:day/:slug/' do
  slug = params[:slug]
  @post = Post.where("slug like ?", slug).first
  halt [ 404, "Page not found" ] unless @post
  @title = @post.title

  erb :post
end

get '/tags/:tag' do
  @tag = params[:tag]
  @posts = Post.where("tags LIKE ?", "%#{@tag}%").reverse_order(:created_at)
  @title = "Posts tagged #{@tag}"
  erb :tagged
end

get '/posts/new' do
  auth
  @post = Post.new
  @url = "/posts"
  erb :edit
end

post '/posts' do
  auth
  @post = Post.new :title => params[:post][:title], :tags => params[:post][:tags], :author => params[:post][:author], :body => params[:post][:body], :slug => Post.make_slug(params[:post][:title])
  if @post.save
    redirect @post.url
  else
    erb :edit
  end
end

get '/:year/:month/:day/:slug/edit' do
  auth
  @post = Post.where("slug LIKE ?", params[:slug]).first
  @url = @post.url
  halt[ 404, "Page not found" ] unless @post
  erb :edit
end

post '/:year/:month/:day/:slug/' do
  auth
  post = Post.where("slug LIKE ?", params[:slug]).first
  halt [ 404, "Page not found" ] unless post
  
  post.title = params[:post][:title]
  post.tags = params[:post][:tags]
  post.author = params[:post][:author]
  post.body = params[:post][:body]
  post.slug = Post.make_slug(params[:post][:title])
  
  post.save
  
  redirect post.url
end

get '/' do
  @posts = Post.limit(10)
  erb :"home"
end

get '/about-us' do
  erb :"about-us"
end

get '/past' do
  @posts = Post.all
  @title = "Archive"
  erb :archive
end

#rss
get '/feed' do
  @posts = Post.last(20).reverse
  content_type 'application/atom+xml', :charset => 'utf-8'
  builder :feed
end

get '/rss' do
  redirect '/feed', 301
end
